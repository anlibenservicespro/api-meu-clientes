"""clientes migrate

Revision ID: 09268d1b901d
Revises: 
Create Date: 2022-07-30 23:08:09.409688

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "09268d1b901d"
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        'clientes',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('nome', sa.String(50), nullable=False),
        sa.Column('whatsapp', sa.String(50), nullable=False),
        sa.Column('estabelecimento', sa.String(50), nullable=False),
        sa.Column('pedido', sa.String(50), nullable=False),
    )


def downgrade() -> None:
    op.drop_table('clientes')
