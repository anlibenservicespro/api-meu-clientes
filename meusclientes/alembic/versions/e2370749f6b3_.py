"""empty message

Revision ID: e2370749f6b3
Revises: 09268d1b901d
Create Date: 2022-07-30 23:12:21.718079

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "e2370749f6b3"
down_revision = "09268d1b901d"
branch_labels = None
depends_on = None


def upgrade() -> None:
    pass


def downgrade() -> None:
    pass
