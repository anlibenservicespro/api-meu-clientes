from fastapi import APIRouter

from app.api.api_v1.endpoints import login, clientes


api_router = APIRouter()
api_router.include_router(login.router, tags=['login'])
api_router.include_router(clientes.router, prefix='/clientes', tags=['clientes'])
# api_router.include_router(login.router, prefix='/login', tags=['login'])
