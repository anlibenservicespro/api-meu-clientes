from fastapi import APIRouter
from app.db.init_db import session_maker
from app.schemas.clientes import ClientesSchemaBase
from fastapi.responses import JSONResponse

router = APIRouter()

@router.get("/clientes/all")
def cliente_all():

    array_clientes = []

    with session_maker() as session:
        clientes = session.query(ClientesSchemaBase).all()
        for cliente in clientes:
            array_clientes.append(cliente)
    
    return JSONResponse(content=array_clientes)

