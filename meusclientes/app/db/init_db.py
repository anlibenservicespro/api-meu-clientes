from sqlalchemy import create_engine
from sqlalchemy.orm.session import sessionmaker


session_maker = sessionmaker(bind=create_engine('sqlite:///models.db'))
