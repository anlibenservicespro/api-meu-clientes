
from typing import Optional

from pydantic import BaseModel

class ClientesSchemaBase(BaseModel):
    nome: str
    whatsapp: str
    estabelecimento: str
    pedido: str
