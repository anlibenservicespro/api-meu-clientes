#!/usr/bin/env bash

set -e
set -x

mypy meusclientes
flake8 meusclientes tests
black meusclientes tests --check
isort meusclientes tests docs_src scripts --check-only